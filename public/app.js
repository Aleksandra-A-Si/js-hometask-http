'use strict';
/*1. Допишите функцию addUserFetch, используя метод fetch и учитывая следующие детали:
    - Функция должна принимать информацию о пользователе в виде js-объекта с полями, описывающими пользователя. 
    Cреди них не должно быть поля "id". Функция должна возвращать промис.
    - Добавлять пользователя на сервер нужно HTTP-методом POST, по урлу "/users", заголовок Content-Type должен иметь значение "application/json". 
    - В случае успешного добавления пользователя, сервер пришлет ответ с только что добавленными данными в формате JSON, и в этом объекте будет 
    содержаться "id" пользователя. 
    Функция должна распарсить полученный JSON, и зарезолвить промис значением id добавленного пользователя;
    - В случае ошибки, например при передаче в функцию addUserXHR не объекта, функция должна реджектиться с ошибкой, в тексте которой должен содержаться
     код ошибки из ответа сервера и его текстовое описание.*/

function addUserFetch(userInfo) {  
  const options = {
    method: 'POST',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify(userInfo)
  }
  return fetch('/users', options).then( response =>{
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject(`${response.status} ${response.statusText}`)
    }
  }  
  ).then(user => Promise.resolve(user.id))
}
//addUserFetch({"lastname": "Sidorova","name": "Aleksandra" })

/*
2. Допишите функцию getUserFetch, используя метод fetch и учитывая следующие детали:
    - Функция должна принимать на вход число — id пользователя. Функция должна возвращать промис.
    - Получить пользователя можно по урлу "/users/{id}", HTTP-метод для получения — GET.
    - В случае успешного запроса пользователя, сервер пришлет ответ содержащий JSON c информацией о пользователе.
    Функция должна распарсить полученный JSON, и зарезолвить промис полученным js-объектом.
    - В случае ошибки, например при передаче в функцию getUserFetch id несуществующего пользователя, функция должна 
    реджектиться с ошибкой, в тексте которой должен содержаться код ошибки из ответа сервера и его текстовое описание.
*/
function getUserFetch(id) {
  return fetch(`/users/${id}`).then( response =>{
    if (response.ok) {
      return response.json();
    } else {
      return Promise.reject(`${response.status} ${response.statusText}`)
    }
  }  
  ).then(user => Promise.resolve(user))
}
//getUserFetch(1)

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) =>{
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '/users');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.addEventListener('load', () =>{
      if(xhr.status < 400) {
        resolve(JSON.parse(xhr.responseText).id)
      } else {
        reject(`${xhr.status} ${xhr.statusText}`)
      }
    });
    xhr.send(JSON.stringify(userInfo));
  })
}
//addUserXHR({ "lastname": "Sidorova_XHR","name": "Aleksandra_XHR" });
function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) =>{
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/users/${id}`);
    xhr.setRequestHeader('Content-Type', 'application/json');    
    xhr.addEventListener('load', () =>{
      if(xhr.status < 400) {
        resolve(JSON.parse(xhr.responseText))
      } else {
        reject(`${xhr.status} ${xhr.statusText}`)
      }
    });
    xhr.send();
  })
}
//getUserXHR(3);
/*
функция проверки функций addUserFetch и getUserFetch

function checkWork() {  
  addUserFetch({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserFetch(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();*/

function checkWork() {  
  addUserXHR({ name: "Alice_2", lastname: "XHR" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork();